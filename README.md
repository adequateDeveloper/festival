# Festival Project

**Following: https://www.compose.com/articles/ecto-2-0-the-database-tool-for-elixir-and-phoenix/**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `festival` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:festival, "~> 0.1.0"}]
    end
    ```

  2. Ensure `festival` is started before your application:

    ```elixir
    def application do
      [applications: [:festival]]
    end
    ```

