defmodule Festival.Repo.Migrations.CreateFestival do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :pwd, :string
    end

    create table(:rating_types) do
      add :name, :string
      add :description, :string
    end

    create table(:sort_types) do
      add :name, :string
      add :description, :string
    end

    create table(:artists) do
      add :name, :string
      add :mb_id, :string
      add :date_formed, :string
    end

    create table(:ratings) do
      add :rating, :integer
      add :user_id, references(:users)
      add :artist_id, references(:artists)
      add :rating_type_id, references(:ratings)
    end

    create table(:sorts) do
      add :ordinal, :integer
      add :artist_id, references(:artists)
      add :sort_type_id, references(:sort_types)
    end
  end

end
