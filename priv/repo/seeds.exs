
data = [
  {:users, [
    {"hays@compose.io", "blah_blah"},
    {"ac@acenterprises.com", "blah_blah_blah"}
  ]},
  {:artists, [
    {"Pearl Jam", "83b9cbe7-9857-49e2-ab8e-b57b01038103", "1990-12-31"},
    {"Dead & Company", "", ""},
    {"LCD Soundsystem", "2aaf7396-6ab8-40f3-9776-a41c42c8e26b", "2001-12-31"},
    {"J. Cole", "8752031e1-8e58-4b86-8dcb-7190faf411c5", "1985-01-28"},
    {"Ellie Goulding", "33ca19f4-18c8-4411-98df-ac23890ce9f5", "1986-12-30"},
    {"Macklemore & Ryan Lewis", "97b226c8-7140-4da3-91ac-15e62e131a81", "2009-12-31"},
    {"Tame Impala (Late Night)", "", ""},
    {"Death Cab for Cutie", "0039c7ae-e1a7-4a7d-9b49-0cbc716821a6", "1997-12-31"}
  ]},
  {:rating_types, [
    {"hays_nps", "on a scale of 0 to 10 would you recommend this artist to friends"}
  ]}
]

defmodule Seeds do
  alias Festival.Repo
  alias Festival.User
  alias Festival.Artist
  alias Festival.RatingType

  def import_data([]), do: nil
  def import_data([head | tail]) do
    case head do
      {:users, vals} -> import_users(vals)
      {:artists, vals} -> import_artists(vals)
      {:rating_types, vals} -> import_rating_types(vals)
    end
    import_data tail
  end

  defp import_users([]), do: nil
  defp import_users([{email, password} | tail]) do
    Repo.insert!(%User{email: email, pwd: password}, log: false)
    import_users tail
  end

  defp import_artists([]), do: nil
  defp import_artists([{name, mb_id, date_formed} | tail]) do
    Repo.insert!(%Artist{name: name, mb_id: mb_id, date_formed: date_formed}, log: false)
    import_artists tail
  end

  defp import_rating_types([]), do: nil
  defp import_rating_types([{name, description} | tail]) do
    Repo.insert!(%RatingType{name: name, description: description}, log: false)
    import_rating_types tail
  end

end

Seeds.import_data data

# To Load: $ mix run priv/repo/seeds.exs