defmodule Festival.User do
  use Ecto.Schema

  # Defines the db table that this schema maps to.
  schema "users" do
    field :email, :string
    field :pwd, :string
    has_many :ratings, Rating
  end
  
end