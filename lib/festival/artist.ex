defmodule Festival.Artist do
  use Ecto.Schema

  # Defines the db table that this schema maps to.
  schema "artists" do
    field :name, :string
    field :mb_id, :string
    field :date_formed, :string
    has_many :ratings, Rating
  end
  
end