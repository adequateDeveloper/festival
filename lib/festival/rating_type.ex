defmodule Festival.RatingType do
  use Ecto.Schema

  # Defines the db table that this schema maps to.
  schema "rating_types" do
    field :name, :string
    field :description, :string
    has_many :ratings, Rating
  end
  
end