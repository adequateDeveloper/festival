defmodule Rating do
  use Ecto.Schema

  # Defines the db table that this schema maps to.
  schema "ratings" do
    field :rating, :integer
    belongs_to :user, User
    belongs_to :artist, Artist
    belongs_to :rating_type, RatingType
  end
  
end