defmodule Festival do
  use Application
  import Ecto.Query
  alias Festival.Repo

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      # Starts a worker by calling: Festival.Worker.start_link(arg1, arg2, arg3)
      # worker(Festival.Worker, [arg1, arg2, arg3]),

      worker(Repo, [])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Festival.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def users_query do
    query = from u in Festival.User,
      select: u
    Repo.all query
  end

  def artists_query do
    query = from a in Festival.Artist,
      select: a
    Repo.all query
  end

end
